<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_licence.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_licence_description' => 'Ajoute la possibilité de sélectionner la licence d’un objet depuis son formulaire d’édition si cette option est activée dans le masque de formulaire de Diogène',
	'diogene_licence_nom' => 'Diogène - Licence',
	'diogene_licence_slogan' => 'Complément "licence" pour "Diogène"'
);
