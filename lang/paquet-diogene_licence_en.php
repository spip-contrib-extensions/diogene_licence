<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_licence?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_licence_description' => 'Enable to select the license of an object from its edit form if this option is enabled in the form mask of Diogene',
	'diogene_licence_nom' => 'Diogene - License',
	'diogene_licence_slogan' => '"License" add-on for "Diogene"'
);
