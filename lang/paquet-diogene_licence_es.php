<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_licence?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_licence_description' => 'Añade la posibilidad de seleccionar la licencia de un objeto desde su formulario de edición si esta opción está activada en la plantilla de formulario de Diógenes',
	'diogene_licence_nom' => 'Diógenes - Licencia',
	'diogene_licence_slogan' => 'Complemento "licencia" para "Diógenes"'
);
