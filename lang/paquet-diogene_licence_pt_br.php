<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_licence?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_licence_description' => 'Incluir a possibilidade de selecionar a licença de um objeto a partirdo seu formulário de edição, se esta opcão estiver ativa na máscara de formulário do Diogenes',
	'diogene_licence_nom' => 'Diogenes - Licença',
	'diogene_licence_slogan' => 'Complemento "licença" para o "Diogenes"'
);
